import { NestFactory } from '@nestjs/core';
import { AppModule } from './modules/app/app.module';
import { ConfigService } from '@nestjs/config';

NestFactory.create(AppModule).then(async (app) => {
  const config = app.get<ConfigService>(ConfigService);
  const port = +config.get<string>('API_PORT');

  await app.listen(port);
});
