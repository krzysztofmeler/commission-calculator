import { Injectable } from '@nestjs/common';
import { TransactionDBRepository } from '../infrastructure/transaction.db-repository';
import { Currency } from '../domain/currency';
import { randomBytes } from 'crypto';

export interface CreateTransactionData {
  date: Date;
  amount: number;
  currency: Currency;
  client_id: number;
}

export interface CommissionDataData {
  currency: Currency;
  amount: number;
}

@Injectable()
export class CreateTransactionService {
  constructor(
    // TODO: Use injection token for DI
    private readonly transactionRepository: TransactionDBRepository,
  ) {}

  async createTransaction(
    data: CreateTransactionData,
  ): Promise<CommissionDataData> {
    // TODO: Calculate commission.
    await this.transactionRepository.save({
      clientId: data.client_id,
      ...data,
      id: randomBytes(400).toString('base64url').slice(0, 36), // TODO: Replace with v4 or/and create factory
    });
    return;
  }
}
