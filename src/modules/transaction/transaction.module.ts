import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TransactionOrmEntity } from './infrastructure/transaction.orm-entity';
import { CreateTransactionController } from './interface/create-transaction.controller';
import { CreateTransactionService } from './application/create-transaction.service';
import { TransactionDBRepository } from './infrastructure/transaction.db-repository';

@Module({
  imports: [TypeOrmModule.forFeature([TransactionOrmEntity])],
  providers: [TransactionDBRepository, CreateTransactionService],
  controllers: [CreateTransactionController],
})
export class TransactionModule {}
