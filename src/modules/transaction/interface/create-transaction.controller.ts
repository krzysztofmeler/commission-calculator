import { Body, Controller, Post } from '@nestjs/common';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { CommissionDataDto } from './dto/commission-data.dto';
import { CreateTransactionService } from '../application/create-transaction.service';

@Controller()
export class CreateTransactionController {
  constructor(
    private readonly createTransactionService: CreateTransactionService,
  ) {}

  @Post('/transactions')
  async createTransaction(
    @Body() data: CreateTransactionDto,
  ): Promise<CommissionDataDto> {
    return this.createTransactionService.createTransaction(data);
  }
}
