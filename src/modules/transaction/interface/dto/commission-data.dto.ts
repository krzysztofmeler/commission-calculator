import { Currency } from '../../domain/currency';
import { CommissionDataData } from '../../application/create-transaction.service';

export class CommissionDataDto implements CommissionDataData {
  currency: Currency;
  amount: number;
}
