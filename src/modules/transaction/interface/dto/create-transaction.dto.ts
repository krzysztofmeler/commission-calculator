import {
  IsDate,
  IsEnum,
  IsInt,
  IsNumber,
  IsPositive,
  Max,
  Min,
} from 'class-validator';
import { Currency } from '../../domain/currency';
import { CreateTransactionData } from '../../application/create-transaction.service';

export class CreateTransactionDto implements CreateTransactionData {
  @IsDate()
  date: Date;

  @Max(100000000)
  @IsPositive()
  @IsNumber()
  amount: number;

  @IsEnum(Currency)
  currency: Currency;

  @Min(0)
  @IsInt()
  client_id: number;
}
