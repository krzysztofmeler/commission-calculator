import { Currency } from './currency';

export interface Transaction {
  id: string;
  date: Date;
  amount: number;
  currency: Currency;
  clientId: number;
}
