import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Currency } from '../domain/currency';

@Entity('transaction')
export class TransactionOrmEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'date' })
  date: Date;

  @Column({ type: 'float', unsigned: true })
  amount: number;

  @Column({ enum: Currency, type: 'enum', nullable: false })
  currency: Currency;

  @Column({ unsigned: true, nullable: false, name: 'client_id' })
  clientId: number;
}
