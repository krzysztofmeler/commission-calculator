import { Transaction } from '../domain/transaction';
import { Repository } from 'typeorm';
import { TransactionOrmEntity } from './transaction.orm-entity';
import { InjectRepository } from '@nestjs/typeorm';

export class TransactionDBRepository {
  constructor(
    @InjectRepository(TransactionOrmEntity)
    private readonly repository: Repository<TransactionOrmEntity>,
  ) {}

  async save(transaction: Transaction): Promise<void> {
    const entity = new TransactionOrmEntity();
    Object.assign<TransactionOrmEntity, TransactionOrmEntity>(entity, {
      ...transaction,
    });
    await this.repository.save(entity);
  }

  async getMany(clientId: number): Promise<Transaction[]> {
    const entities = await this.repository.find({
      where: {
        clientId,
      },
    });

    return entities.map((entity) => ({
      ...entity,
    }));
  }
}
