import {
  TypeOrmModuleAsyncOptions,
  TypeOrmModuleOptions,
} from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TransactionOrmEntity } from '../modules/transaction/infrastructure/transaction.orm-entity';

export const TypeormModuleConfig: TypeOrmModuleAsyncOptions = {
  imports: [ConfigModule],
  inject: [ConfigService],
  useFactory: (config: ConfigService): TypeOrmModuleOptions => ({
    host: config.get<string>('DB_HOST'),
    port: +config.get<string>('DB_PORT'),
    username: config.get<string>('DB_USER'),
    password: config.get<string>('DB_PASSWORD'),
    database: config.get<string>('DB_NAME'),
    synchronize: true,
    type: 'mysql',
    retryAttempts: 2,
    retryDelay: 300,
    entities: [TransactionOrmEntity],
  }),
};
